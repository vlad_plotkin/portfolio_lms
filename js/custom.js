var block = ""
Notify = {				
    TYPE_INFO: 0,				
    TYPE_SUCCESS: 1,				
    TYPE_WARNING: 2,				
    TYPE_DANGER: 3,								

    generate: function (aText, aOptHeader, aOptType_int) {					
        var lTypeIndexes = [this.TYPE_INFO, this.TYPE_SUCCESS, this.TYPE_WARNING, this.TYPE_DANGER];					
        var ltypes = ['alert-info', 'alert-success', 'alert-warning', 'alert-danger'];										
        var ltype = ltypes[this.TYPE_INFO];					

        if (aOptType_int !== undefined && lTypeIndexes.indexOf(aOptType_int) !== -1) {						
            ltype = ltypes[aOptType_int];					
        }										

        var lText = '';					
        if (aOptHeader) {						
            lText += "<h4>"+aOptHeader+"</h4>";					
        }					
        lText += aText;										
        var lNotify_e = jQuery("<div class='alert "+ltype+"'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>"+lText+"</div>");					

        setTimeout(function () {						
            lNotify_e.alert('close');					
        }, 4000);					
        lNotify_e.appendTo($("#save-status"));				
    }			
};		
            
jQuery(document).ready( function() {
	jQuery('#mainform').state_form();
	jQuery.ajax({
           type : "post",
           dataType : "html",
           url : myAjax.ajaxurl,
           data : {action:'single_block'},
           success: function(response) {
				block = response
		  }
	})
	    jQuery.ajax({
               type : "post",
               dataType : "html",
               url : myAjax.ajaxurl,
               data : {action:'multi_block',block:1},
               success: function(response) {
				    jQuery('#selector_form0').append(response)
		      }
	    })
	    jQuery.ajax({
               type : "post",
               dataType : "html",
               url : myAjax.ajaxurl,
               data : {action:'multi_block',block:2},
               success: function(response) {
				    jQuery('#selector_form1').append(response)
		      }
	    })
	    jQuery.ajax({
               type : "post",
               dataType : "html",
               url : myAjax.ajaxurl,
               data : {action:'multi_block',block:3},
               success: function(response) {
				    jQuery('#selector_form2').append(response)
		      }
	    })
	    jQuery.ajax({
               type : "post",
               dataType : "html",
               url : myAjax.ajaxurl,
               data : {action:'multi_block',block:4},
               success: function(response) {
				    jQuery('#selector_form3').append(response)
		      }
	    })
    jQuery('#user_vote').validator().on('click', function (e) {
        if (e.isDefaultPrevented()) {
            alert('Не верные данные выделены красным цветом')
        } else {
           var o = []
           o[0] = ''
           if (jQuery('#mainform').state_form('is_changed')) {
                o[0] = jQuery('#mainform').state_form('get_changes')}
           for(i=0; i<4; i++){
            o[i+1] = jQuery('#selector_form'+i).serializeArray()
           }
           jQuery.ajax({
               type : "post",
               dataType : "json",
               url : myAjax.ajaxurl,
               data : {action:'my_user_vote',data:o},
               success: function(response) {
				if(response.success == 'true') Notify.generate("<strong>Успешно!</strong> Изменения сохранены",false, 1)
				else Notify.generate("<strong>Внимание!</strong> Значения не изменены",false, 2);
              }
           }) 
           }  
    })

   jQuery(".addButton").on("click", function(){
        var id = jQuery(this).attr("name").charAt(3)
        var nBlock = 0
       	if (jQuery("#selector_form"+id).children().is(".mainBlock")){
       	    var lastBlock = jQuery("#selector_form"+id).children(".mainBlock").last()
		    nBlock = Number(lastBlock.attr('name').charAt(7))+1
		    console.log(nBlock)
		}
     	jQuery("#selector_form"+id).append(block)
     	var lastBlock = jQuery("#selector_form"+id).children(".mainBlock").last()
     	lastBlock.attr('name','chBlock'+nBlock)
     	lastBlock.find('[name = wp_form_concurs]').attr('name', ("wp_form_concurs"+nBlock))
		lastBlock.find('[name = wp_dost_concurs]').attr('name', ("wp_dost_concurs"+nBlock))
		lastBlock.find('[name = wp_mesto_concurs]').attr('name', ("wp_mesto_concurs"+nBlock))
   })
   
})
