<?php
/*
Plugin Name: Портфолио КПКУ
Description: Создание формы для ведения портфолио
Version: 1.0
Author: Влад Плоткин
Author URI: http://mail.kpku.ru/
Plugin URI: 
*/

define('PORTFOLIO_DIR', plugin_dir_path(__FILE__));

function anketa_load(){
    require_once(PORTFOLIO_DIR.'includes/main_window.php');
}
anketa_load();
//require_once(PORTFOLIO_DIR.'includes/pole.php');
require_once(PORTFOLIO_DIR.'includes/action.php');
function load_bootstrap(){
    wp_enqueue_script('bootstrap-js', WP_PLUGIN_URL.'/portfolio/bootstrap/js/bootstrap.js');
    wp_enqueue_style('bootstrap-css', WP_PLUGIN_URL.'/portfolio/bootstrap/css/bootstrap.css');
    wp_enqueue_style('notify-css', WP_PLUGIN_URL.'/portfolio/css/notify.css');
    wp_enqueue_script('jquery-js', WP_PLUGIN_URL.'/portfolio/form-state/jquery.js');
    wp_enqueue_script('bootstrap-alert-js', WP_PLUGIN_URL.'/portfolio/bootstrap/js/bootstrap-alert.js');
	wp_enqueue_script('jquery-form-js', WP_PLUGIN_URL.'/portfolio/form-state/jquery.form_state.js');
	wp_enqueue_script('validator-js', WP_PLUGIN_URL.'/portfolio/js/validator.js');
	

}
add_action('wp_enqueue_scripts', 'load_bootstrap');
global $lmsdb;
require_once(PORTFOLIO_DIR.'config.php');
$lmsdb = new wpdb( $user_lms, $password_lms, $database_lms,$server_lms );

add_filter('user_contactmethods', 'my_user_contactmethods');
function my_user_contactmethods($user_contactmethods){
  $user_contactmethods['mid_name'] = 'Отчество';
  $user_contactmethods['mid'] = 'Идентификатор LMS';
  return $user_contactmethods;
}
?>
