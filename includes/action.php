<?php
add_action( 'init', 'my_script_enqueuer' );
function my_script_enqueuer() {
   wp_register_script( "my_voter_script", WP_PLUGIN_URL.'/portfolio/js/custom.js', array('jquery') );
   wp_localize_script( 'my_voter_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        
   wp_enqueue_script( 'jquery' );
   wp_enqueue_script( 'my_voter_script' );
}
add_action("wp_ajax_my_user_vote", "my_user_vote"); 

function my_user_vote() {
   global $lmsdb;
   $current_user = wp_get_current_user();
   $mid = get_user_meta($current_user->ID,'mid',true);
   $data = $_REQUEST["data"];
   if($lmsdb->get_var($lmsdb->prepare('SELECT COUNT(*) as c FROM `xp_portfolio` WHERE `mid` = %d and `year` = (SELECT `school_year`.`xp_key` FROM `school_year` ORDER BY `school_year`.`xp_key` DESC LIMIT 1)',$mid),0,0) < 1){
        $lmsdb->query($lmsdb->prepare("INSERT INTO `studium`.`xp_portfolio` (`year`,`mid`) VALUES ((SELECT `school_year`.`xp_key` FROM `school_year` ORDER BY `school_year`.`xp_key` DESC LIMIT 1),%d)",$mid));
   }
    //$rows = $wpdb->get_row('SELECT lms_name FROM wp_block_name WHERE NOT `group` = -1', ARRAY_N, 0);
	//$type = array('mid','I_Family','ILike','InPeople','InCurYear','IPride','LikeCourse','IWantYear','IMast','MyDO','IHopeDO',' Vpechat1','Dumayu1','Vpechat2','Dumayu2','Vpechat3','Dumayu3','Dumayu4','Vpechat4','Dost1','Dost2','Dost3','Dost4','KD_Edu','KD_Sport','KD_Social','KD_Other','KD_I','MCH_1','MCH_2','MCH_3','MCH_4','MCH_5','MCH_6','MCH_7','MCH_8');
   if(!empty($data[0])){
       foreach ($data[0] as $value) { 
            $pole[$value['element_name']] = esc_sql($value['curent_val']);
       }
   }
   for($i=1 ;$i<5; $i++){
    if(empty($data[$i])) $data[$i]=0;
    $index = 'Dost'.$i;
    $pole[$index] = json_encode($data[$i]);
   }
   $status = ($lmsdb->update( 'xp_portfolio', $pole, array( 'MID' => $mid , 'year' => '9' )) <> 0)?"true":"false";
   echo '{"success":"'.$status.'"}';
   die();
 
}
add_action("wp_ajax_multi_block", "my_multi_block" );
function my_multi_block(){
    $nBlock = 1;
    if(isset($_REQUEST["block"])) $nBlock = $_REQUEST["block"];
    global $lmsdb;
    $current_user = wp_get_current_user();
    $mid = get_user_meta($current_user->ID,'mid',true);
    $jsonString = $lmsdb->get_var($lmsdb->prepare('SELECT Dost%d as c FROM `xp_portfolio` WHERE `mid` = %d and `year` = (SELECT `school_year`.`xp_key` FROM `school_year` ORDER BY `school_year`.`xp_key` DESC LIMIT 1)',array($nBlock,$mid)),0,0);
    
    $chunk = array_chunk(json_decode($jsonString), 8);
    $valueInNewArray = array();
    foreach($chunk as $block=>$value){
        foreach($value as $key=>$name){
           $valueInNewArray[$key] = $name->value; 
        }
        my_single_block($valueInNewArray,$block);
   }
   die();
}

add_action("wp_ajax_single_block", "my_single_block");

function my_single_block($input, $nBlock){
    $single=false;
    if(empty($input)){ $single=true; $input = array("","","","","","дистанционно","лауреат","I");};
	global $wpdb;
	?>

<div class="row mainBlock" name="chBlock<?php echo $nBlock; ?>">
	<div class="form-group col-12">
		<label class="col-form-label" style="text-align:center; font-size:10pt;">Официальное название конкурса</label><button style="align: right" type="button" onclick='jQuery(this).parent().parent(".mainBlock").remove()' class="close delButton"" aria-hidden="true">&times;</button>
		<select style="font-size:10pt; height: 35px" class="input-sm form-control col-12" name="wp_concurs_list">

			<?php
				$result = $wpdb->get_col('SELECT `Name` FROM `wp_concurs` ORDER BY `Name`;',0);
				array_unshift($result, "");
				foreach($result as $key=>$value){
					$attr = ($key == $input[0])?"selected":"";
					echo "<option value=\"".$key."\" ".$attr.">".$value."</option>";
				}
			?>
		</select>
	</div>
	<?php
	$punkt = array('wp_predmet'=>'Предмет','wp_level_concurs'=>'Уровень','Дата','Педагог');
	$i = 1;
	foreach($punkt as $key=>$elem){
	?>
	<div class="form-group col-6">
		<label class="col-form-label" style="text-align:center; font-size:10pt;"><?php echo $elem;?></label>
		<select style="font-size:10pt; height: 35px" class="form-control" name="<?php echo $key?>">
			<?php
				$result = $wpdb->get_col('SELECT `Name` FROM `'.$key.'` ORDER BY `Name`;',0);
				array_unshift($result, "");
				foreach($result as $key=>$value){
				$attr = ($value == $input[$i])?"selected":"";
				echo "<option value=\"".$value."\" ".$attr.">".$value."</option>";
				}
				$i++;
			?>
		</select>
	</div>
	<?php
	}
	?>
	<?php 
	$punkt2 = array('wp_form_concurs'=>'Форма проведения','wp_dost_concurs'=>'Уровень результата','wp_mesto_concurs'=>'Место');
	foreach($punkt2 as $key=>$elem){
	?>
	<div class="form-group col-4">
		<label class="col-form-label" style="font-size:10pt"><?php echo $elem;?></label>
		<select style="font-size:10pt; height: 35px" class="form-control" name="<?php echo $key?>">
			<?php
				$result = $wpdb->get_col('SELECT `Name` FROM `'.$key.'` ORDER BY `Name`;',0);
				array_unshift($result, "");
				foreach($result as $key=>$value){
				$attr = ($value == $input[$i])?"selected":"";
				echo "<option value=\"".$value."\" ".$attr.">".$value."</option>";
				}
				$i++;
			?>
		</select>
	</div>
	<?php
	}
	/*
	$punkt2 = array('wp_form_concurs'=>'Форма проведения','wp_dost_concurs'=>'Уровень результата','wp_mesto_concurs'=>'Место');
	$i=5;
	foreach($punkt2 as $key=>$value){
	?>
		<div class="col-md-4"><b style="font-size:10pt"><?php echo $value;?></b>
			<div class="row">
			   <!-- <div class="btn-group btn-group-vertical">-->
			   <?php
			    $result = $wpdb->get_col('SELECT `Name` FROM `'.$key.'` ORDER BY `Name`;',0);
	
			    foreach($result as $key2=>$value2){
			        $attr = ($value2 == $input[$i])?"checked=enable":"";
				    echo "<input class='col-3' type='radio' name='$key$nBlock' value='$value2' ".$attr."><div class='col-9'>".$value2."</div>";
			    }
			    $i++;
			?>			
			</div>
	    </div>
	<?php
	}*/
	?>
</div>
<?php

if($single) die();
}

?>
