<?php
add_shortcode('portfolio', 'portfolio_window');
function portfolio_window(){
    if ( is_user_logged_in() ) {
        global $wpdb;
        global $lmsdb;
        global $lms;
        $current_user = wp_get_current_user();
        define('PORTFOLIO_DIR', plugin_dir_path(__FILE__));
        include_once(PORTFOLIO_DIR.'data.php');
        $tab_list = $wpdb->get_results('SELECT value, lms_name FROM `wp_block_name` WHERE `group` = -1', ARRAY_A);
        $mid = get_user_meta($current_user->ID,'mid',true);
        $lms = $lmsdb->get_results('SELECT mid,I_Family, ILike, InPeople, InCurYear, IPride, LikeCourse, IWantYear, IMast, MyDO, IHopeDO,  Vpechat1, Dumayu1, Vpechat2, Dumayu2, Vpechat3, Dumayu3, Dumayu4, Vpechat4, Dost1, Dost2, Dost3, Dost4, KD_Edu, KD_Sport, KD_Social, KD_Other, KD_I, MCH_1, MCH_2, MCH_3, MCH_4, MCH_5, MCH_6, MCH_7, MCH_8 FROM `xp_portfolio` WHERE `mid` = "'.$mid.'" and year =  (SELECT `school_year`.`xp_key` FROM `school_year` ORDER BY `school_year`.`xp_key` DESC LIMIT 1) LIMIT 1;', ARRAY_A);
        ?>
        <div class="row" style="font-size:12pt">
			<div class="col-12" id="save-status"></div>
            <div class="col-12" style="font-size:7.5pt">
                <form id="mainform" data-toggle="validator" role="form" method=post action="#">
                    
                        <div class="row">
                            <!-- Nav tabs -->
                            <div class="col-12 form-group">
                                <ul class="nav nav-tabs" role="tablist">
                                <?php 
                                    foreach ($tab_list as $key => $value){
                                    if(isset($_REQUEST["position"])) {
                                        $position = $_REQUEST["position"];
                                    $attr = ($position == $value['lms_name'])?'active':'';
                                    }
                                    else $attr = ($key == 0)?'active':'';
                                ?>                
                                  <li class="nav-item" style="list-style-type: none">
                                    <a name="<?php echo $value['lms_name']; ?>" class="nav-link <?php echo $attr;?>" data-toggle="tab" href="#portfolio_data_<?php echo $key;?>" role="tab"><?php echo $value['value'];?></a>
                                  </li>
                                <?php 
                                };
                                ?>
                                </ul>
                            <!-- Tab panes -->
                                <br>
                                <div class="tab-content">
                                <?php 
                                    foreach ($tab_list as $key => $value){
                                        if(isset($_REQUEST["position"])) {
                                        $position = $_REQUEST["position"];
                                        $attr = ($position == $value['lms_name'])?'active':'';
                                    }
                                    else $attr = ($key == 0)?'active':'';
                                ?>    
                                  <div class="form-group tab-pane <?php echo $attr;?> show" id="portfolio_data_<?php echo $key;?>" role="tabpanel" style="font-size:10pt"><?php data($key);?></div>
                                <?php
                                    };
                                ?>
                                </div>
                           </div>
                           <div class="col-12 form-group">
                                 <input type="button" id="user_vote" class="btn btn-xs btn-primary" value ="Сохранить" >
	                        </div>
	                    </div>
                </form>
            </div>
        </div>
        <?php 
    }else {
            require_once(PORTFOLIO_DIR.'html/welcome.html');
        }
}

function standart_area($array){
    global $lms;
    foreach ($array as $key => $value) {
        ?>
            <div class="row form-group">
                <div class="col-3">
                    <label for="<?php echo $value['lms_name'];?>"><?php echo $value['value'];?></label>
                </div>
                <div class="col-9"> <!-- pattern="^[_A-z0-9а-яА-Я.\s]{1,}$"-->
                    <textarea style="font-size:8pt" id="<?php echo $value['lms_name'];?>" class="form-control"  data-error="Не верные данные" <?php echo ($value['lms_name'] == "Dost1" or $value['lms_name'] == "Dost2" or $value['lms_name'] == "Dost3" or $value['lms_name'] == "Dost4" )?"hidden":"" ?> ><?php echo $lms[0][$value['lms_name']]; ?></textarea>
                    <?php if($value['lms_name'] == "Dost1" or $value['lms_name'] == "Dost2" or $value['lms_name'] == "Dost3" or $value['lms_name'] == "Dost4" ){ ?>
                    <div id="selector_<?php echo $key;?>">
						<form></form>
						<form class="blockForm" id="selector_form<?php echo $key;?>">
						</form>
					</div>
                    <input type="button" class="btn btn-primary btn-sm addButton" name="add<?php echo $key;?>" value="Добавить конкурс"><!--<input type="button" class="delButton" name="del<?php echo $key;?>" value="Удалить конкурс">-->
                    <?php }?>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        <?php
        };
}
function data($block){
    global $wpdb;
    $array = $wpdb->get_results($wpdb->prepare('SELECT `value`, `lms_name` FROM `wp_block_name` WHERE `group` = %d',$block), ARRAY_A);
    if(!empty($array)){standart_area($array); }
    else { error();}
}
function error(){
?>

    Страница не создана

<?php
}

?>
